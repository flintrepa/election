#include <iostream>
using namespace std;

#include <integer.h>
#include <osrng.h>
#include <ecp.h>
#include <nbtheory.h>
using CryptoPP::Integer;
using CryptoPP::ECP;

const Integer p("8000000000000000000000000000000000000000000000000000000000000431h")
	, a("7h")
	, b("5FBFF498AA938CE739B8E022FBAFEF40563F6E6A3472FC2A514C0CE9DAE23B7Eh")
	, m("8000000000000000000000000000000150FE8A1892976154C59CFC193ACCF5B3h")
	, q("8000000000000000000000000000000150FE8A1892976154C59CFC193ACCF5B3h");

const ECP::Point P(Integer("2h"),
	Integer("8E2A8A0E65147D4BD6316030E16D19C85C97F0A9CA267122B96ABBCEA7E8FC8h"));

const ECP ecp(p, a, b);
const ECP::Point g(P);

Integer rand(const unsigned int size)
{
	CryptoPP::SecByteBlock scratch(size);
	CryptoPP::AutoSeededRandomPool rng;
	rng.GenerateBlock(scratch, scratch.size());

	Integer k;
	k.Decode(scratch.BytePtr(), scratch.SizeInBytes());
	return k;
}


void print(const char* msg, const Integer &i)
{
	cout << msg << " = " << hex << i << endl;
}

void print(const char* msg, const ECP::Point &p)
{
	cout << msg << ".x = " << hex << p.x << endl
		<< msg << ".y = " << hex << p.y << endl;
}

bool Inj(ECP::Point &P, const Integer k, const char* c)
{
	const int bufferSize = 32;
	byte bt[bufferSize] = { 0 };

	k.Encode(bt, 32);

	const byte *cData = (const byte*)c;
	for (int i = 8; i < bufferSize; i++)
	{
		bt[i] ^= cData[i];
	}

	Integer x = 0, y;
	x.Decode(bt, 31);
	x %= p;
	y = ((x*x + a)*x + b) % p;

	y = ModularSquareRoot(y, p);
	if (x % 2 == y % 2)
	{
		y = p - y;
	}

	P = ECP::Point(x, y);
	return ecp.VerifyPoint(P);
}

void main(void)
{
	assert(ecp.VerifyPoint(g));
	
	/* blind the data */
	Integer k, ssk = rand(32);
	ECP::Point inj;

	do
	{
		k = rand(32);
	} while (!Inj(inj, k, "Kennedy vs Nixon"));
	print("k", k);

	ECP::Point h = ecp.Multiply(ssk, g);
	//ECP::Point t = ecp.Add(inj, h);

	ECP::Point t(Integer("275ac39a1fc82da40926a61f48b38b618a38daeef3b9d71dfe24e6aaed5f00ech")
		, Integer("37f7a42646cd56fa4c19c8f1e635d90c49884a623c09fe597766d8b49ff7886fh"));

	/* blind signing */
	Integer sk("1e78c8eb220ac27cea140594d15da4679a0b5545af4e5cd8befe7cb5e5216b8dh"); //= rand(32);
	ECP::Point pk = ecp.Multiply(sk, g);

	ECP::Point st = ecp.Multiply(sk, t);
	print("st", st);

	/* check signature */
	ECP::Point s = ecp.Subtract(st, ecp.Multiply(ssk, pk));
	//assert(s == ecp.Multiply(sk, inj));
}