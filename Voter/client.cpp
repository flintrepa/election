#include <QtNetwork>

#include "client.h"
#include "../VotingCenter/params.h"

Client::Client(Voter *voter, QObject *parent)
	: tcpSocket(new QTcpSocket(this))
	, networkSession(Q_NULLPTR)
	, voter(voter)
{
	connect(tcpSocket, &QIODevice::readyRead, this, &Client::read);
	typedef void (QAbstractSocket::*QAbstractSocketErrorSignal)(QAbstractSocket::SocketError);
	connect(tcpSocket, static_cast<QAbstractSocketErrorSignal>(&QAbstractSocket::error),
		this, &Client::displayError);

	connect(this, SIGNAL(answerReceived(QByteArray&)), voter, SLOT(handleAnswer(QByteArray&)));

	QNetworkConfigurationManager manager;
	if (manager.capabilities() & QNetworkConfigurationManager::NetworkSessionRequired) {
		// Get saved network configuration
		QSettings settings(QSettings::UserScope, QLatin1String("QtProject"));
		settings.beginGroup(QLatin1String("QtNetwork"));
		const QString id = settings.value(QLatin1String("DefaultNetworkConfiguration")).toString();
		settings.endGroup();

		// If the saved network configuration is not currently discovered use the system default
		QNetworkConfiguration config = manager.configurationFromIdentifier(id);
		if ((config.state() & QNetworkConfiguration::Discovered) !=
			QNetworkConfiguration::Discovered) {
			config = manager.defaultConfiguration();
		}

		networkSession = new QNetworkSession(config, this);
		connect(networkSession, &QNetworkSession::opened, this, &Client::sessionOpened);

		qDebug() << tr("Opening network session.");
		networkSession->open();
	}

	tcpSocket->connectToHost(HOST_ADDRESS, SERVER_PORT);
}

Client::~Client()
{
	tcpSocket->close();
}

void Client::request(QByteArray &request)
{
	QByteArray block;
	QDataStream out(&block, QIODevice::WriteOnly);
	out << request;
	
	connect(tcpSocket, &QAbstractSocket::disconnected,
		tcpSocket, &QObject::deleteLater);
	tcpSocket->write(block);
}

void Client::read()
{
	QByteArray incomingData = tcpSocket->readAll();
	buffer.append(incomingData);

	while (buffer.length() >= HEADER_SIZE)
	{
		HeaderSizeType blockSize;
		QDataStream stream(&buffer, QIODevice::ReadOnly);
		stream >> blockSize;

		if (blockSize + HEADER_SIZE <= buffer.length())
		{
			QByteArray block = buffer.mid(HEADER_SIZE, blockSize);
			emit answerReceived(block);
			buffer.remove(0, blockSize + HEADER_SIZE);
		}
	}
}

void Client::handleAnswer(QByteArray & answer)
{
	QString dataAsString;
	QDataStream stream(&answer, QIODevice::ReadOnly);
	stream >> dataAsString;
	//qDebug() << "Received:" << dataAsString;
}

void Client::displayError(QAbstractSocket::SocketError socketError)
{
	switch (socketError) {
	case QAbstractSocket::RemoteHostClosedError:
		break;
	case QAbstractSocket::HostNotFoundError:
		qDebug() << tr("The host was not found. Please check the "
				"host name and port settings.");
		break;
	case QAbstractSocket::ConnectionRefusedError:
		qDebug() << tr("The connection was refused by the peer. "
				"Make sure the voting center server is running, "
				"and check that the host name and port "
				"settings are correct.");
		break;
	default:
		qDebug() << tr("The following error occurred: %1.")
			.arg(tcpSocket->errorString());
	}
}

void Client::sessionOpened()
{
	// Save the used configuration
	QNetworkConfiguration config = networkSession->configuration();
	QString id;
	if (config.type() == QNetworkConfiguration::UserChoice)
		id = networkSession->sessionProperty(QLatin1String("UserChoiceConfiguration")).toString();
	else
		id = config.identifier();

	QSettings settings(QSettings::UserScope, QLatin1String("QtProject"));
	settings.beginGroup(QLatin1String("QtNetwork"));
	settings.setValue(QLatin1String("DefaultNetworkConfiguration"), id);
	settings.endGroup();

	qDebug() << tr("This examples requires that you run the "
		"Voting Center Server as well.");
}