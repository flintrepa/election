﻿#include "voter.h"
#include "signature.h"
#include "inj.h"
#include "cryptoparams.h"
#include "notary.h"

#include <votingstage.h>
#include <operators.h>

#include <iostream>
#include <sstream>
#include <string>
#include <cmath>

#include <osrng.h>
#include <filters.h>
#include <algebra.h>
#include <hex.h>
using CryptoPP::StringSink;
using CryptoPP::StringStore;
using CryptoPP::StringSource;
using CryptoPP::ArraySink;
using CryptoPP::SignerFilter;
using CryptoPP::HexEncoder;
using CryptoPP::HexDecoder;

#include <QDebug>

#include <string>
using std::string;

Voter::Voter(QByteArray *c, Integer v)
	: client(new Client(this))
	, c(c)
	, v(v)
{
}

Voter::~Voter()
{
	delete client;
}


void Voter::handleAnswer(QByteArray &data)
{
	QDataStream stream(&data, QIODevice::ReadOnly);
	VotingStage stage;
	stream >> stage;

	QString answer;

	switch (stage)
	{
	case DSA_PUBLIC_KEY_TRANSFER:
	case BLIND_SIGNING:
		stream >> answer;
		qDebug() << answer;
		break;

	case BLIND_SIGNATURE_SENT:
		signedBlanksReceived(stream);
		break;

	case PROVABLY_SINGED:
		provablySignedBlanksReceived(stream);
		break;

	case REQUEST_SERVER_BLINDING_PK:
		blindingPKReceived(stream);
		break;

	case CAST_VOTE:
		signedVoteReceived(stream);
		break;

	default:
		qDebug() << "Invlaid message format.";
	}
}

void Voter::run()
{
	testSigningAlgorithm();
	//testLegendre(ECP_PARAM::p);

	/* генерация и передача публичного ключа цифровой подписи */
	generateDSAKeys();
	sendDSAPublicKey();

	/* 2 */
	sendBlanksForBlindSigning();
}


void Voter::testSigningAlgorithm()
{
	Integer d("7A929ADE789BB9BE10ED359DD39A72C11B60961F49397EEE1D19CE9891EC3B28h");
	ECP::Point Q(
		Integer("7F2B49E270DB6D90D8595BEC458B50C58585BA1D4E9B788F6689DBD8E56FD80Bh"),
		Integer("26F1B489D6701DD185C8413A977B3CBBAF64D1C593D26627DFFB101A87FF77DAh"));
	
	Signature s;
	QByteArray M("wubalubadubdub");
	ECSignature signature = s.sign(M, d);
	
	assert(s.checkSignature(M, signature));
}

void Voter::generateDSAKeys()
{
	CryptoPP::AutoSeededRandomPool rng;
	sK.GenerateRandomWithKeySize(rng, 1024);
	pK.AssignFrom(sK);
	assert(sK.Validate(rng, 3) && pK.Validate(rng, 3));
}

void Voter::sendDSAPublicKey()
{
	QByteArray data;
	QDataStream stream(&data, QIODevice::WriteOnly);
	stream << DSA_PUBLIC_KEY_TRANSFER << i;

	string encodedPublicKey;
	pK.Save(StringSink(encodedPublicKey).Ref());

	stream << encodedPublicKey;

	client->request(data);
}

void Voter::sendBlanksForBlindSigning()
{
	/* сериализация t */
	std::ostringstream os;

	int kSize = 16, sskSize = 32;
	for (int i = 0; i < 3; i++)
	{
		ECP::Point injP;
		int cnt = 0;
		do
		{
			k[i] = Signature::rand(kSize);
			++cnt;
		} while (!Inj(injP, k[i], c));

		ssk[i] = Signature::rand(sskSize);
		h[i] = EC.Multiply(ssk[i], G);

		t[i] = EC.Add(injP, h[i]);
		//qDebug() << cnt;
		os << t[i].x << t[i].y;
	}

	DSA::Signer signer(sK);
	CryptoPP::AutoSeededRandomPool rng;
	string signature;
	StringSource as(os.str(), true,
		new SignerFilter(rng, signer,
			new StringSink(signature)
		)
	);

	QByteArray data;
	QDataStream dataStream(&data, QIODevice::WriteOnly);
	dataStream << BLIND_SIGNING << i;
	for (int i = 0; i < 3; i++)
	{
		dataStream << t[i];
	}
	dataStream << signature;

	client->request(data);
}

void Voter::signedBlanksReceived(QDataStream & stream)
{
	for (int i = 0; i < 3; i++)
	{
		stream >> st[i] >> pk[i];
		s[i] = EC.Subtract(st[i], EC.Multiply(ssk[i], pk[i]));
	}
	qDebug() << "Signed blanks received";
	
	/* 2 */
	sendBlanksForProvableSignature();
}

void Voter::sendBlanksForProvableSignature()
{
	QByteArray data;
	QDataStream stream(&data, QIODevice::WriteOnly);
	stream << PROVABLE_SIGNING << *c;

	for (int i = 0; i < 3; i++)
	{
		stream << k[i] << s[i];
	}
	client->request(data);
}

void Voter::provablySignedBlanksReceived(QDataStream &stream)
{
	Signature signature;
	for (int i = 0; i < 3; i++)
	{
		stream >> S[i];
		
		Integer x;
		bool b = inj(x, k[i], c);
		assert(b);

		if (!signature.checkSignature(x, S[i]))
		{
			qDebug() << "Provable signature is invalid";
			return;
		}
	}
	qDebug() << "Provable signatures OK";

	/* 3 */
	requestServerBlindingPK();
}

void Voter::requestServerBlindingPK()
{
	QByteArray data;
	QDataStream stream(&data, QIODevice::WriteOnly);
	stream << REQUEST_SERVER_BLINDING_PK;
	client->request(data);
}

void Voter::blindingPKReceived(QDataStream & stream)
{
	for (int i = 0; i < 3; i++)
	{
		stream >> PK[i];
	}
	qDebug() << "Blinding multipliers received";
	vote();
}

void Voter::vote()
{
	js = Signature::rand(32) % 3;

	do
	{
		SSK = Signature::rand(32) % (ECP_PARAM::q - 1);
	} while (CryptoPP::EuclideanDomainOf<Integer>().Gcd(SSK, ECP_PARAM::q - 1) != 1);
	SPK = MAq.Exponentiate(PK[js], SSK);

	u = Signature::rand(32) % ECP_PARAM::p;

	Integer jopp = Signature::rand(32) % 3;
	bool validVote = false;
	while (!validVote)
	{
		for (int i = 0; i < 3; i++)
		{
			ECP::Point P;
			do
			{
				x[i] = getRandLegendre(v * (jopp == i ? -1 : 1));
				ux[i] = u * x[i];
			} while (!Inj(P, k[i], c, ux[i], S[i]));
		}
		for (int i = 0; i < 3; i++)
		{
			for (int j = i + 1; j < 3; j++)
			{
				if (legendre(ux[i] * ux[j]) == -1)
				{
					validVote = true;
				}
			}
		}
	}

	for (int i = 0; i < 3; i++)
	{
		ub[i] = Ballot(*c, S[i], ux[i]);
	}

	// TODO: шифровать данные для передачи
	QByteArray data;
	QDataStream stream(&data, QIODevice::WriteOnly);
	stream << CAST_VOTE;
	for (int i = 0; i < 3; i++)
	{
		stream << k[i] << s[i] << ub[i];
	}
	stream << SPK;
	client->request(data);
}

void Voter::signedVoteReceived(QDataStream & stream)
{
	ECSignature C[3];
	Ballot ubReceived[3];
	for (int i = 0; i < 3; i++)
	{
		stream >> ubReceived[i] >> C[i];
		assert(ubReceived[i] == ub[i]);
	}

	/* снятие затемнения */
	Integer m = MAq.Divide(1, MAq.Exponentiate(ECP_PARAM::G, SSK));
	Sj.r = MAq.Multiply(C[js].r, m);
	Sj.s = MAq.Multiply(C[js].s, m);
	Sj.pk = C[js].pk;

	/* проверка подписи */
	Signature signature;
	Integer b;
	bool res = inj(b, k[js], c, ux[js], S[js]);
	assert(res);
	if (!signature.checkSignature(b, Sj))
	{
		qDebug() << "Ballot's provable signature is invlaid";
		return;
	}
	qDebug() << "Ballot's provable signature OK";

	/* 4 */
	sendBlindingKeys();
}

void Voter::sendBlindingKeys()
{
	QByteArray data;
	QDataStream stream(&data, QIODevice::WriteOnly);
	stream << SEND_BLINDING_KEY << *c;
	for (int i = 0; i < 3; i++)
	{
		stream << k[i] << S[i];
	}

	/* шифрование u */
	std::ostringstream os;
	os << std::hex << u;
	string uStr = os.str();

	CryptoPP::AutoSeededRandomPool rng;
	assert(Notary::getInstance().getPK().Validate(rng, 3));
	
	string uCipher;
	CryptoPP::RSAES_OAEP_SHA_Encryptor e(Notary::getInstance().getPK());
	
	StringSource ss1(uStr, true,
		new CryptoPP::PK_EncryptorFilter(rng, e,
			new HexEncoder(
				new StringSink(uCipher)
			) // HexEncoder
		) // PK_EncryptorFilter
	); // StringSource

	stream << uCipher;

	client->request(data);

	qDebug() << "\nDONE";
}
