﻿#pragma once
#include <QObject>
#include "signature.h"
#include "ballot.h"

class Client;
#include "client.h"

#include <integer.h>
using CryptoPP::Integer;

#include <ecp.h>
using CryptoPP::ECP;

#include <dsa.h>
using CryptoPP::DSA;

class Voter : public QObject
{
	Q_OBJECT

private:
	Client *client;

	/* Свойства выборов */
	QByteArray *c; /* краткая форма вопроса */
	Integer v; /* голос избирателя ([-1] - за первого кандидата, [+1] - за второго) */

	/* Свойства избирателя */
	Integer i = 123456; /* ИД избирателя */
	Integer k[3]; /* конфиденциальные идентификаторы */
	Integer ssk[3]; /* сеансовые секреты */
	ECP::Point h[3]; /* затемняющие сдвиги */
	ECP::Point t[3]; /* затемнённые идентификаторы вопросов бюллетеней */

	ECP::Point st[3]; /* слепая подпись сервера затемненных бланков */
	ECP::Point s[3]; /* слепая подпись сервера */

	ECP::Point pk[3]; /* открытый ключ подписи сервера */

	ECSignature S[3]; /* доказательная подпись бланков бюллетеней */

	Integer PK[3]; /* затемняющие множители сервера */
	int js; /* выбранный для доказательной подписи бюллетень */
	Integer SSK; /* секретный ключ клиента */
	Integer SPK; /* публичный ключ клиента */
	Integer x[3]; /* голос избирателя */
	Integer u; /* затемняющий множитель голоса */
	Integer ux[3]; /* затемненные голоса */
	Ballot ub[3]; /* затемненные бюллетени */

	ECSignature Sj; /* доказательно подписанный бюллетень */

	DSA::PrivateKey sK;
	DSA::PublicKey pK;
	
	void generateDSAKeys();
	void sendDSAPublicKey();

	/* 1 - генерирование и слепая подпись «для сервера»
	идентификаторов анонимных персональных бланков бюллетеней */
	void sendBlanksForBlindSigning();
	void signedBlanksReceived(QDataStream &stream);

	/* 2 - доказательная подпись анонимных бланков бюллетеней */
	void sendBlanksForProvableSignature();
	void provablySignedBlanksReceived(QDataStream &stream);

	/* 3 - голосование */
	void requestServerBlindingPK();
	void blindingPKReceived(QDataStream &stream);
	void vote();
	void signedVoteReceived(QDataStream &stream);

	/* 4 - вручение затемняющих множителей */
	void sendBlindingKeys();

public:
	Voter(QByteArray *c, Integer v);
	~Voter();

	void run();

	/* Test */
	void testSigningAlgorithm();

public slots:
	void handleAnswer(QByteArray &data);
};
