#include <QtCore/QCoreApplication>
#include "client.h"
#include "voter.h"
#include "operators.h"
#include "inj.h"
#include "signature.h"

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);
	
	QByteArray c;
	QDataStream stream(&c, QIODevice::WriteOnly);
	stream << QString("Kennedy Nixon");

	Voter voter(&c, -1);
	voter.run();
	
	return a.exec();
}
