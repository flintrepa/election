#pragma once
#include <QtNetwork>

class Voter;
#include "voter.h"

class Client : public QObject
{
	Q_OBJECT

public:
	explicit Client(Voter *voter, QObject *parent = Q_NULLPTR);
	~Client();

	void request(QByteArray &request);
	
private slots:
	void displayError(QAbstractSocket::SocketError socketError);
	void sessionOpened();
	void read();
	void handleAnswer(QByteArray &answer);

signals:
	void answerReceived(QByteArray &data);

private:
	QTcpSocket *tcpSocket;
	QNetworkSession *networkSession;

	QByteArray buffer;
	Voter *voter;
};