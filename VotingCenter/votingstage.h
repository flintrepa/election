﻿#pragma once

#include <QDataStream>

/* Стадии протокола голосования */
typedef enum
{
	/* передача публичного ключа цифровой подписи */
	DSA_PUBLIC_KEY_TRANSFER
	
	/* 1 - генерирование и слепая подпись «для сервера»
	идентификаторов анонимных персональных бланков бюллетеней */
	, BLIND_SIGNING
	/* отправка бланков, подписанных вслепую сервером, 
	обратно клиенту */
	, BLIND_SIGNATURE_SENT

	/* 2 - доказательная подпись анонимных бланков бюллетеней */
	, PROVABLE_SIGNING
	, PROVABLY_SINGED

	/* 3 - голосование */
	, REQUEST_SERVER_BLINDING_PK
	, CAST_VOTE

	/* 4 - вручение затемняющих множителей */
	, SEND_BLINDING_KEY
} VotingStage;

