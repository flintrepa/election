#pragma once
#include "signature.h"
#include "cryptoparams.h"

#include <QByteArray>

#include <integer.h>
using CryptoPP::Integer;

#include <ecp.h>
using CryptoPP::ECP;

bool Inj(ECP::Point &P, const Integer k, const QByteArray* c);
bool inj(Integer &i, const Integer k, const QByteArray* c);

bool Inj(ECP::Point &P
	, const Integer k
	, const QByteArray* c
	, const Integer &x
	, const ECSignature &s);

bool inj(Integer &i
	, const Integer k
	, const QByteArray* c
	, const Integer &x
	, const ECSignature &s);

Integer legendre(Integer a, Integer p = ECP_PARAM::p);
void testLegendre(Integer p);
Integer getRandLegendre(Integer l);