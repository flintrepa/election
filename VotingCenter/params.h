#pragma once

typedef quint32 HeaderSizeType;
#define HEADER_SIZE ((int)sizeof(HeaderSizeType))

QHostAddress HOST_ADDRESS = QHostAddress::LocalHost; //QHostAddress("169.254.69.64");
quint16 SERVER_PORT = 22761;