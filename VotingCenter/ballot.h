#pragma once
#include "signature.h"

#include <QByteArray>

typedef struct BallotTAG
{
	QByteArray c;
	ECSignature S;
	Integer ux;

	BallotTAG() : c(), S(), ux("0") {};
	BallotTAG(QByteArray c, ECSignature S, Integer ux) : c(c), S(S), ux(ux) {};
} Ballot;