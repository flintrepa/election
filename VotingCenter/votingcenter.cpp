﻿#include "votingcenter.h"
#include "operators.h"
#include "cryptoparams.h"
#include "inj.h"
#include "signature.h"
#include "notary.h"

#include <iostream>
#include <vector>
#include <string>
#include <sstream>

#include <filters.h>
#include <osrng.h>
#include <hex.h>
using CryptoPP::StringStore;
using CryptoPP::StringSource;
using CryptoPP::ArraySink;
using CryptoPP::StringSink;
using CryptoPP::SignatureVerificationFilter;
using CryptoPP::HexDecoder;
using CryptoPP::HexEncoder;

Integer rand(const unsigned int size)
{
	CryptoPP::SecByteBlock scratch(size);
	CryptoPP::AutoSeededRandomPool rng;
	rng.GenerateBlock(scratch, scratch.size());

	Integer k;
	k.Decode(scratch.BytePtr(), scratch.SizeInBytes());
	return k;
}

VotingCenter::VotingCenter()
	: server(new Server(this))
{ 
	for (int i = 0; i < 4; i++)
	{
		sk[i] = rand(32);
		pk[i] = EC.Multiply(sk[i], G);
	}
}

VotingCenter::~VotingCenter()
{
	delete server;
}

void VotingCenter::handleRequest(QByteArray &data)
{
	QDataStream stream(&data, QIODevice::ReadOnly);
	VotingStage stage;
	stream >> stage;

	switch (stage)
	{
	case DSA_PUBLIC_KEY_TRANSFER:
		qDebug() << "\n--> DSA pk transfer";
		DSAPublicKeyTransfer(stream);
		break;

	case BLIND_SIGNING:
		qDebug() << "\n--> Blind signing";
		blindSigning(stream);
		break;

	case PROVABLE_SIGNING:
		qDebug() << "\n--> Provable signing";
		provableSigning(stream);
		break;

	case REQUEST_SERVER_BLINDING_PK:
		generateBlindingKeys();
		sendBlindingPK();
		break;

	case CAST_VOTE:
		qDebug() << "\n--> Vote casting";
		castingVote(stream);
		break;

	case SEND_BLINDING_KEY:
		qDebug() << "\n--> Blinding key received";
		blindingKeyReceived(stream);
		break;

	default:
		qDebug() << "Invlaid message format.";
	}
}

void VotingCenter::DSAPublicKeyTransfer(QDataStream &stream)
{
	Integer i;
	std::string encodedPK;
	stream >> i >> encodedPK;
	
	DSA::PublicKey decodedPK;
	decodedPK.Load(
		StringStore(encodedPK).Ref()
	);
		
	pK.insert(i, decodedPK);
	votingStage.insert(i, DSA_PUBLIC_KEY_TRANSFER);

	qDebug() << "DSA public key receiveid";

	QByteArray answerData;
	QDataStream answerStream(&answerData, QIODevice::WriteOnly);
	answerStream << DSA_PUBLIC_KEY_TRANSFER << QString("DSA public key receiveid");
	server->answer(answerData);
}

bool VotingCenter::checkBlindSignature(ECP::Point s, Integer k, Integer sk)
{
	ECP::Point inj;
	if (!Inj(inj, k, &c))
	{
		qDebug() << "Invalid data!";
		return false;
	}
	if (!(s == EC.Multiply(sk, inj)))
	{
		qDebug() << "Blind signature is invalid";
		return false;
	}
	return true;
}

void VotingCenter::blindSigning(QDataStream & stream)
{
	Integer i;
	stream >> i;

	/* проверка наличия открытого ключа для проверки подписи */
	if (!pK.contains(i))
	{
		qDebug() << "No DSA public key";
	}

	/* проверка на какой стадии находится избиратель */
	if (votingStage.value(i, DSA_PUBLIC_KEY_TRANSFER) >= BLIND_SIGNING)
	{
		qDebug() << "Voter is already on a further stage";
		QByteArray answerData;
		QDataStream answerStream(&answerData, QIODevice::WriteOnly);
		answerStream << BLIND_SIGNING << QString("Denied");
		server->answer(answerData);
		return;
	}

	votingStage.insert(i, BLIND_SIGNING);

	/* проверка подписи */
	ECP::Point t[3];
	std::ostringstream msg;
	for (int i = 0; i < 3; i++)
	{
		stream >> t[i];
		assert(EC.VerifyPoint(t[i]));
		msg << t[i].x << t[i].y;
	}

	std::string signature;
	stream >> signature;

	DSA::Verifier verifier(pK.value(i));
	bool result = false;
	StringSource ss(msg.str() + signature, true,
		new SignatureVerificationFilter(
			verifier,
			new ArraySink(
				(byte*)&result, sizeof(result)),
			SignatureVerificationFilter::Flags::PUT_RESULT 
				| SignatureVerificationFilter::Flags::SIGNATURE_AT_END
		)
	);

	if (!result)
	{
		qDebug() << "Signature is invalid";
		QByteArray answerData;
		QDataStream answerStream(&answerData, QIODevice::WriteOnly);
		answerStream << BLIND_SIGNING << QString("Denied. DSA signature is invalid");
		server->answer(answerData);
		return;
	}

	/* слепая подпись бланков */
	ECP::Point st[3];
	for (int i = 0; i < 3; i++)
	{
		st[i] = EC.Multiply(sk[i], t[i]);
	}

	/* отправка избирателю подписанных бланков */
	QByteArray data;
	QDataStream dataStream(&data, QIODevice::WriteOnly);
	dataStream << BLIND_SIGNATURE_SENT;
	for (int i = 0; i < 3; i++)
	{
		dataStream << st[i] << pk[i];
	}
	server->answer(data);
	votingStage.insert(i, BLIND_SIGNATURE_SENT);
}

void VotingCenter::provableSigning(QDataStream & stream)
{
	ECP::Point s[3];
	Integer k[3];
	quint32 cSize;
	stream >> c;

	for (int i = 0; i < 3; i++)
	{
		stream >> k[i] >> s[i];
		if (!checkBlindSignature(s[i], k[i], sk[i]))
		{
			return;
		}
	}
	qDebug() << "Blind signatures OK";

	QByteArray answer;
	QDataStream answerStream(&answer, QIODevice::WriteOnly);
	Signature signature;

	answerStream << PROVABLY_SINGED;
	for (int i = 0; i < 3; i++)
	{
		Integer x;
		bool b = inj(x, k[i], &c);
		assert(b);
		ECSignature s = signature.sign(x, sk[3]);
		answerStream << s;
	}

	server->answer(answer);
}

void VotingCenter::generateBlindingKeys()
{
	for (int i = 0; i < 3; i++)
	{
		do
		{
			SK[i] = rand(32) % (ECP_PARAM::q - 1);
		} while (CryptoPP::EuclideanDomainOf<Integer>().Gcd(SK[i], ECP_PARAM::q - 1) != 1);
		PK[i] = MAq.Exponentiate(ECP_PARAM::G, SK[i]);
	}
}

void VotingCenter::sendBlindingPK()
{
	QByteArray data;
	QDataStream stream(&data, QIODevice::WriteOnly);
	stream << REQUEST_SERVER_BLINDING_PK;
	for (int i = 0; i < 3; i++)
	{
		stream << PK[i];
	}
	server->answer(data);
}

void VotingCenter::castingVote(QDataStream & stream)
{
	ECP::Point s[3];
	Integer k[3];
	Integer SPK;
	Ballot ub[3];

	Signature signature;

	for (int i = 0; i < 3; i++)
	{
		stream >> k[i] >> s[i] >> ub[i];
		if (!checkBlindSignature(s[i], k[i], sk[i]))
		{
			return;
		}

		Integer x;
		bool b = inj(x, k[i], &ub[i].c);
		assert(b);

		/* проверка доказательной подписи бланков */
		if (!signature.checkSignature(x, ub[i].S))
		{
			qDebug() << "Provable signature is invalid";
			return;
		}
	}
	qDebug() << "Blind signatures OK";
	qDebug() << "Provable signature OK";

	stream >> SPK;

	/* проверка правильность голосования */
	bool validVote = false;
	for (int i = 0; i < 3; i++)
	{
		for (int j = i + 1; j < 3; j++)
		{
			if (legendre(ub[i].ux * ub[j].ux) == -1)
			{
				validVote = true;
			}
		}
	}
	if (!validVote)
	{
		qDebug() << "Invalid vote";
		return;
	}
	qDebug() << "Vote OK";

	/* сохранение голосов */
	for (int i = 0; i < 3; i++)
	{
		ballots.insert(k[i], ub[i]);
	}

	/* доказательная подпись заполненных бюллетеней (одного) */
	ECSignature C[3];
	for (int i = 0; i < 3; i++)
	{
		Integer m = MAq.Exponentiate(SPK, MAq1.Divide(1, SK[i]));
		Integer b;

		bool res = inj(b, k[i], &c, ub[i].ux, ub[i].S);
		assert(res);
		
		ECSignature bS = signature.sign(b, sk[i]);
		C[i].r = MAq.Multiply(bS.r, m);
		C[i].s = MAq.Multiply(bS.s, m);
		C[i].pk = bS.pk;
	}
	qDebug() << "Ballots signed";

	QByteArray data;
	QDataStream dataStream(&data, QIODevice::WriteOnly);
	dataStream << CAST_VOTE;
	for (int i = 0; i < 3; i++)
	{
		dataStream << ub[i] << C[i];
	}
	server->answer(data);
}

void VotingCenter::blindingKeyReceived(QDataStream & stream)
{
	Integer k[3];
	ECSignature S[3];
	QByteArray c;
	std::string uCipher;
	stream >> c;
	for (int i = 0; i < 3; i++)
	{
		stream >> k[i] >> S[i];
	}
	stream >> uCipher;

	/* проверки */
	Signature signature;
	for (int i = 0; i < 3; i++)
	{
		Integer x;
		bool b = inj(x, k[i], &c);
		assert(b);

		/* проверка наличия бюллетеней у сервера */
		if (!ballots.contains(k[i]))
		{
			qDebug() << "Invalid k";
			return;
		}

		/* проверка доказательной подписи бланков */
		if (!signature.checkSignature(x, S[i]))
		{
			qDebug() << "Provable signature is invalid";
			return;
		}

		Ballot savedBallot = ballots.value(k[i]);
		if (savedBallot.c != c || !(savedBallot.S == S[i]))
		{
			qDebug() << "Invalid data";
			return;
		}
	}

	qDebug() << "Data OK";

	for (int i = 0; i < 3; i++)
	{
		encBlindingKeys.insert(k[i], uCipher);
	}

	decryptBlindingKeys();
}

void VotingCenter::decryptBlindingKeys()
{
	qDebug() << "\n--> Decrypting blinding key";
	
	CryptoPP::AutoSeededRandomPool rng;
	assert(Notary::getInstance().getSK().Validate(rng, 3));

	for (QMap<Integer, std::string>::Iterator i = encBlindingKeys.begin()
		; i != encBlindingKeys.end()
		; ++i)
	{
		std::string uStr;
		CryptoPP::RSAES_OAEP_SHA_Decryptor d(Notary::getInstance().getSK());

		StringSource ss2(i.value(), true,
			new HexDecoder(
				new CryptoPP::PK_DecryptorFilter(rng, d,
					new StringSink(uStr)
				) // PK_DecryptorFilter
			) // HexDecoder
		); // StringSource

		decBlindingKeys.insert(i.key(), Integer(uStr.c_str()));
	}
	qDebug() << "All blinding keys decrypted";

	tally();
}

void VotingCenter::tally()
{
	qDebug() << "\n--> Tallying";
	
	Integer bin[2] = { 0 };
	Signature signature;
	for (QMap<Integer, Ballot>::Iterator i = ballots.begin()
		; i != ballots.end()
		; ++i)
	{
		if (!decBlindingKeys.contains(i.key()))
		{
			qDebug() << "No blinding key found";
			return;
		}

		Integer injRes;
		bool b = inj(injRes, i.key(), &i.value().c);
		assert(b);
		if (!signature.checkSignature(injRes, i.value().S))
		{
			qDebug() << "Provable signature is invalid";
			return;
		}

		Integer ux = i.value().ux
			, u = decBlindingKeys.value(i.key())
			, x = ux / u
			, l = legendre(x);

		if (l == -1)
		{
			bin[0]++;
		}
		else if (l == 1)
		{
			bin[1]++;
		}
		else
		{
			qDebug() << "Invalid vote";
			return;
		}
	}

	Integer voterCount = ballots.size() / 3;
	bin[0] -= voterCount;
	bin[1] -= voterCount;

	qDebug() << "RESULTS";
	print("for candidate 1", bin[0]);
	print("for candidate 2", bin[1]);
}
