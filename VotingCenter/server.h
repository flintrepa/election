﻿#pragma once

#include <QObject>
#include <QtNetwork>

class VotingCenter;
#include "votingcenter.h"

class Server : public QObject
{
	Q_OBJECT

public:
	explicit Server(VotingCenter *vc, QObject *parent = Q_NULLPTR);
	~Server();
	
private slots:
	void sessionOpened();
	void newConnection();
	void read();

public slots:
	void answer(QByteArray &data);
	void answer(QString &data);

signals:
	void requestReceived(QByteArray &data);

private:
	QTcpServer *tcpServer;
	QTcpSocket *clientConnection;
	QNetworkSession *networkSession;

	QByteArray buffer;
	VotingCenter *votingCenter;
};
