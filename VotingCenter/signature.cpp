﻿#include "signature.h"
#include "cryptoparams.h"
#include "operators.h"

#include <vector>

#include <sha.h>
#include <hex.h>
#include <osrng.h>

#include <qdatastream.h>

#include <iostream>
#include <stdlib.h>
#include <sstream>
#include <string>

Signature::Signature()
	: p(ECP_PARAM::p)
	, a(ECP_PARAM::a)
	, b(ECP_PARAM::b)
	, m(ECP_PARAM::m)
	, q(ECP_PARAM::q)
	, P(ECP_PARAM::P)
	, ecp(EC)
	, group(q)
{
	/* значения параметров взяты из примера 1 из ГОСТа */
	assert(m % q == 0);
	assert((4*a*a*a + 27*b*b) % p != 0);
}


Signature::~Signature()
{
}


ECSignature Signature::sign(const QByteArray & M, const Integer & d)
{
	Q = EC.Multiply(d, P);

	/* 1 - хэш-код сообщения M */
	const byte* data = (byte*)M.data();
	byte h[CryptoPP::SHA256::DIGESTSIZE];
	CryptoPP::SHA256().CalculateDigest(h, data, M.size());

	/* 2 - целое число альфа, e */
	Integer alpha(h, CryptoPP::SHA256::DIGESTSIZE);
	Integer e = alpha % q;
	if (e == 0)
	{
		e = 1;
	}

	Integer r, k, s;
	ECP::Point C;
	do
	{
		do
		{
			/* 3 - генерация псевдослучайного 0 < k < q */
			k = rand(16);
			assert(k < q);

			/* 4 - вычисление точки эллиптической кривой C=kP, вычисление r */
			C = ecp.Multiply(k, P);
			r = C.x % q;
		} while (r == 0);

		/* 5 - вычисление s */
		s = (r*d + k*e) % q;
	} while (s == 0);

	/* 6 - конкатенация r и s */
	QByteArray signature;
	QDataStream stream(&signature, QIODevice::WriteOnly);
	stream << r << s;
	
	return ECSignature(r, s, Q);
}

ECSignature Signature::sign(const Integer & i, const Integer & d)
{
	QByteArray data;
	QDataStream stream(&data, QIODevice::WriteOnly);
	stream << Integer(i);
	return sign(data, d);
}


Integer Signature::rand(const unsigned int size)
{
	CryptoPP::SecByteBlock scratch(size);
	CryptoPP::AutoSeededRandomPool rng;
	rng.GenerateBlock(scratch, scratch.size());

	Integer k;
	k.Decode(scratch.BytePtr(), scratch.SizeInBytes());
	return k;
}


bool Signature::checkSignature(const QByteArray &M, ECSignature &signature)
{
	/* 1 - получение r и s */
	Integer r = signature.r
		  , s = signature.s;
	ECP::Point Q = signature.pk;

	/* 2 - хэш-код сообщения */
	const byte* data = (byte*)M.data();
	byte h[CryptoPP::SHA256::DIGESTSIZE];
	CryptoPP::SHA256().CalculateDigest(h, data, M.size());

	/* 3 - целое число альфа, e */
	Integer alpha(h, CryptoPP::SHA256::DIGESTSIZE);
	Integer e = alpha % q;
	if (e == 0)
	{
		e = 1;
	}

	/* 4 - вычислить v */
	Integer v = group.Divide(1, e);
	
	/* 5 - вычислить z1, z2 */
	Integer z1 = group.Multiply(s, v);
	Integer z2 = group.Multiply(group.Multiply(r, v), -1);

	/* 6 - вычислить точку эллиптической кривой C, получить R */
	ECP::Point C = ecp.Add(ecp.Multiply(z1, P), ecp.Multiply(z2, Q));
	Integer R = C.x % q;

	/* 7 - проверка R=r */
	return R == r;
}

bool Signature::checkSignature(const Integer & i, ECSignature & signature)
{
	QByteArray data;
	QDataStream stream(&data, QIODevice::WriteOnly);
	stream << Integer(i);
	return checkSignature(data, signature);
}

