﻿#include "inj.h"
#include "operators.h"

#include <iostream>
#include <QDebug>

#include <nbtheory.h>

ECP::Point makePoint(byte* bt, size_t bufferSize);
void gen2param(byte* bt, size_t bufferSize, const Integer k, const QByteArray* c);

int inline min(int a, int b)
{
	return a <= b ? a : b;
}

bool Inj(ECP::Point &P, const Integer k, const QByteArray* c)
{
	const int bufferSize = 32;
	byte bt[bufferSize] = { 0 };

	gen2param(bt, bufferSize, k, c);
	
	P = makePoint(bt, bufferSize);
	return EC.VerifyPoint(P);
}

/* генераци я использованием первых двух параметров */
void gen2param(byte* bt, size_t bufferSize, const Integer k, const QByteArray* c)
{
	k.Encode(bt, 32);

	const byte *cData = (const byte*)c->data();
	for (int i = 8; i < min(bufferSize, c->size()); i++)
	{
		bt[i] ^= cData[i];
	}
}

ECP::Point makePoint(byte* bt, size_t bufferSize)
{
	Integer p(ECP_PARAM::p)
		, a(ECP_PARAM::a)
		, b(ECP_PARAM::b);

	Integer x = 0, y;
	x.Decode(bt, bufferSize - 1);
	x %= p;
	y = ((x*x + a)*x + b) % p;

	y = ModularSquareRoot(y, p);
	if (x % 2 == y % 2)
	{
		y = p - y;
	}

	return ECP::Point(x, y);
}

bool inj(Integer & i, const Integer k, const QByteArray * c)
{
	ECP::Point P;
	bool res = Inj(P, k, c);
	i = P.x;
	return res;
}

Integer legendre(Integer a, Integer p) 
{
	CryptoPP::ModularArithmetic group(p);
	Integer l = group.Exponentiate(a, (p - 1) / 2);
	return (l == p - 1) ? -1 : l;
}

Integer getRandLegendre(Integer l)
{
	Integer res;
	do
	{
		res = Signature::rand(32);
	} while (legendre(res, ECP_PARAM::p) != l);
	return res;
}

void testLegendre(Integer p)
{
	for (int i = 0; i < 20; i++)
	{
		Integer a = Signature::rand(32);
		print("[a]_p", legendre(a, p));
	}
}

bool Inj(ECP::Point & P, const Integer k, const QByteArray * c, const Integer & x, const ECSignature & s)
{
	const int bufferSize = 32;
	byte bt[bufferSize] = { 0 };

	gen2param(bt, bufferSize, k, c);

	for (int i = 16; i < bufferSize; i++)
	{
		bt[i] ^= x.GetByte(i - 16);
	}

	for (int i = 24; i < bufferSize; i++)
	{
		bt[i] ^= (s.r.GetByte(i - 24) & s.s.GetByte(i - 24));
	}

	P = makePoint(bt, bufferSize);
	return EC.VerifyPoint(P);
}

bool inj(Integer & i, const Integer k, const QByteArray * c, const Integer & x, const ECSignature & s)
{
	ECP::Point P;
	bool res = Inj(P, k, c, x, s);
	i = P.x;
	return res;
}
