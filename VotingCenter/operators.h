#pragma once
#include "votingstage.h"
#include "signature.h"
#include "ballot.h"

#include <QDataStream>

#include <integer.h>
using CryptoPP::Integer;

#include <ecp.h>
using CryptoPP::ECP;

#include <string>

QDataStream& operator >> (QDataStream& in, VotingStage& e);

QDataStream& operator << (QDataStream& in, Integer &i);
QDataStream& operator >> (QDataStream& in, Integer &i);

QDataStream& operator << (QDataStream& in, ECP::Point &p);
QDataStream& operator >> (QDataStream& in, ECP::Point &p);

QDataStream& operator << (QDataStream& in, std::string &s);
QDataStream& operator >> (QDataStream& in, std::string &s);

QDataStream& operator << (QDataStream& in, ECSignature &s);
QDataStream& operator >> (QDataStream& in, ECSignature &s);
bool operator ==(Ballot &a, Ballot &b);

QDataStream& operator << (QDataStream& in, Ballot &b);
QDataStream& operator >> (QDataStream& in, Ballot &b);
bool operator ==(ECSignature &a, ECSignature &b);
bool operator !=(ECSignature &a, ECSignature &b);

void print(std::string msg, const Integer &i);
void print(std::string msg, const ECP::Point &p);
void print(std::string msg, const ECSignature &s);
