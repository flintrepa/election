#include "operators.h"

#include <iostream>
#include <sstream>

QDataStream& operator >> (QDataStream& in, VotingStage& e)
{
	quint32 tmp;
	in >> tmp;
	e = VotingStage(tmp);
	return in;
}

QDataStream& operator << (QDataStream& in, Integer &i)
{
	std::vector<unsigned char> iVec;
	iVec.resize(i.MinEncodedSize());
	i.Encode(&iVec[0], iVec.size());
	in.writeBytes((const char*)iVec.data(), iVec.size());
	return in;
}

QDataStream& operator >> (QDataStream& in, Integer &i)
{
	int iSize;
	in >> iSize;
	char* iChar = (char*)malloc(iSize);
	in.readRawData(iChar, iSize);
	i.Decode((byte*)iChar, iSize);
	free(iChar);
	return in;
}

QDataStream& operator << (QDataStream& in, ECP::Point &p)
{
	in << p.identity << p.x << p.y;
	return in;
}

QDataStream& operator >> (QDataStream& in, ECP::Point &p)
{
	in >> p.identity >> p.x >> p.y;
	return in;
}

QDataStream& operator << (QDataStream& in, std::string &s)
{
	in << s.size();
	in.writeRawData(s.data(), s.size());
	return in;
}

QDataStream& operator >> (QDataStream& in, std::string &s)
{
	size_t size;
	in >> size;

	char* data = (char*)malloc(size);
	in.readRawData(data, size);
	s = std::string((char*)data, size);
	free(data);
	return in;
}

QDataStream& operator << (QDataStream& in, ECSignature &s)
{
	in << s.r << s.s << s.pk;
	return in;
}

QDataStream& operator >> (QDataStream& in, ECSignature &s)
{
	in >> s.r >> s.s >> s.pk;
	return in;
}

bool operator ==(ECSignature &a, ECSignature &b)
{
	return a.r == b.r && a.s == b.s && a.pk == b.pk;
}

QDataStream& operator << (QDataStream& in, Ballot &b)
{
	in << b.c << b.S << b.ux;
	return in;
}

QDataStream& operator >> (QDataStream& in, Ballot &b)
{
	in >> b.c >> b.S >> b.ux;
	return in;
}

bool operator == (Ballot &a, Ballot &b)
{
	return a.c == b.c && a.S == b.S && a.ux == b.ux;
}

bool operator != (Ballot &a, Ballot &b)
{
	return !(a == b);
}

void print(std::string msg, const Integer &i)
{
	std::cout << msg << " = " << std::hex << i << std::endl;
}

void print(std::string msg, const ECP::Point &p)
{
	std::cout << msg << ".x = " << std::hex << p.x << std::endl
		<< msg << ".y = " << std::hex << p.y << std::endl;
}

void print(std::string msg, const ECSignature &s)
{
	std::cout << msg << ".r = " << std::hex << s.r << std::endl
		<< msg << ".s = " << std::hex << s.s << std::endl;
	print(msg + ".pk", s.pk);
}
