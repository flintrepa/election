﻿#include <QCoreApplication>
#include <QDebug>


#include "server.h"
#include "cryptoparams.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
	
	/* запуск избирательного центра */
	VotingCenter vc;

    return a.exec();
}
