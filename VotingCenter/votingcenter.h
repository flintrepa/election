﻿#pragma once
#include "votingstage.h"
#include "signature.h"
#include "ballot.h"

#include <QObject>
#include <QMap>

#include <integer.h>
using CryptoPP::Integer;

#include <dsa.h>
using CryptoPP::DSA;

#include <ecp.h>
using CryptoPP::ECP;

class Server;
#include "server.h"

class VotingCenter : public QObject
{
	Q_OBJECT

public:
	VotingCenter();
	~VotingCenter();

private:
	Server *server;

	/* Свойства центра */
	Integer sk[4]; /* секретный ключ подписи. sk[3] = sk0 */
	ECP::Point pk[4]; /* открытый ключ подписи. pk[3] = pk0 */
	Integer SK[3]; /* секретный ключ затемнения */
	Integer PK[4]; /* открытый ключ затемнения */

	QByteArray c; /* краткая форма вопроса */
	
	/* Каждому избирателю соспоставим стадию протокола голосования */
	QMap<Integer, VotingStage> votingStage;
	
	/* Каждому избирателю соспоставим открытый ключ цифровой подписи */
	QMap<Integer, DSA::PublicKey> pK;

	/* Затемненные бюллетени */
	QMap<Integer, Ballot> ballots;

	/* Зашифрованные затемняющие множители */
	QMap<Integer, std::string> encBlindingKeys;

	/* Расшифрованные затемняющие множители */
	QMap<Integer, Integer> decBlindingKeys;

	void DSAPublicKeyTransfer(QDataStream &stream);

	/* проверка слепой подписи бланков */
	bool checkBlindSignature(ECP::Point s, Integer k, Integer sk);
	
	/* 1 */
	void blindSigning(QDataStream &stream);
	/* 2 */
	void provableSigning(QDataStream &stream);
	/* 3 */
	void generateBlindingKeys();
	void sendBlindingPK();
	void castingVote(QDataStream &stream);
	/* 4 */
	void blindingKeyReceived(QDataStream &stream);

	/* Расшифровка затемняющих множителей */
	void decryptBlindingKeys();

	/* подведение итогов */
	void tally();

public slots:
	void handleRequest(QByteArray &data);

};

