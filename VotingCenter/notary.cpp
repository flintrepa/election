﻿#include "notary.h"

#include <QDebug>

#include <osrng.h>
#include <files.h>
using CryptoPP::FileSource;
using CryptoPP::FileSink;
using CryptoPP::AutoSeededRandomPool;

RSA::PrivateKey Notary::getSK()
{
	return skN;
}

RSA::PublicKey Notary::getPK()
{
	return pkN;
}

Notary::Notary()
{
	AutoSeededRandomPool rng;
	
	/* загрузка ключей из файлов */
	try 
	{
		FileSource inputSk("../rsaprivate.dat", true)
			, inputPk("../rsapublic.dat", true);
		skN.BERDecode(inputSk);
		pkN.BERDecode(inputPk);

		assert(pkN.Validate(rng, 3) && skN.Validate(rng, 3));
		return;
	}
	catch (CryptoPP::FileStore::OpenErr)
	{
		qDebug() << "No files with RSA keys found";
	}
		
	/* генерация ключей нотариуса */
	skN.GenerateRandomWithKeySize(rng, 1536);
	pkN = RSA::PublicKey(skN);

	assert(pkN.Validate(rng, 3) && skN.Validate(rng, 3));

	/* запись ключей в файлы */
	FileSink outputSk("../rsaprivate.dat");
	FileSink outputPk("../rsapublic.dat");
	pkN.DEREncode(outputPk);
	skN.DEREncode(outputSk);

	qDebug() << "RSA keys generated and saved";
}
