﻿#pragma once

#include <integer.h>
using CryptoPP::Integer;

#include <ecp.h>
using CryptoPP::ECP;

#include <QByteArray>

/** 
	ГОСТ Р 34.10─2012
	Процесс формирования и проверки электронной	цифровой подписи
 */

typedef struct ECSignatureTAG
{
	Integer r, s;
	ECP::Point pk;
	ECSignatureTAG() : r("0"), s("0"), pk(0, 0) {};
	ECSignatureTAG(Integer r, Integer s, ECP::Point pk) : r(r), s(s), pk(pk) {};
} ECSignature;

class Signature
{
private:
	/* Параметры */
	Integer p; /* модуль эллиптической кривой */
	Integer a, b; /* коэффициенты эллиптической кривой */
	Integer m; /* порядок группы точек эллиптической кривой */
	Integer q; /* порядок циклической подгруппы группы точек эллиптической кривой */
	ECP::Point P; /* точка P эллиптической кривой Е, с координатами (x_p , y_p), удовлетворяющая равенству qP=O. */

	Integer d; /* ключ подписи */
	ECP::Point Q; /* ключ проверки подписи – точка эллиптической кривой с координатами (x_q, y_q), удовлетворяющей равенству dP = Q. */

	ECP ecp; /* эллиптическая кривая */
	CryptoPP::ModularArithmetic group; /* группа по модулю */
	
public:
	Signature();
	~Signature();

	ECSignature sign(const QByteArray &M, const Integer &d);
	ECSignature sign(const Integer &i, const Integer &d);

	bool checkSignature(const QByteArray &M, ECSignature &signature);
	bool checkSignature(const Integer &i, ECSignature &signature);

	static Integer rand(const unsigned int size);

	
};

