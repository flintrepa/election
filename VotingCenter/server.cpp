﻿#include <QtNetwork>

#include "server.h"
#include "params.h"

Server::Server(VotingCenter *vc, QObject *parent)
	: tcpServer(Q_NULLPTR)
	, networkSession(0)
	, votingCenter(vc)
{
	QNetworkConfigurationManager manager;
	if (manager.capabilities() & QNetworkConfigurationManager::NetworkSessionRequired) {
		// Get saved network configuration
		QSettings settings(QSettings::UserScope, QLatin1String("QtProject"));
		settings.beginGroup(QLatin1String("QtNetwork"));
		const QString id = settings.value(QLatin1String("DefaultNetworkConfiguration")).toString();
		settings.endGroup();

		// If the saved network configuration is not currently discovered use the system default
		QNetworkConfiguration config = manager.configurationFromIdentifier(id);
		if ((config.state() & QNetworkConfiguration::Discovered) !=
			QNetworkConfiguration::Discovered) {
			config = manager.defaultConfiguration();
		}

		networkSession = new QNetworkSession(config, this);
		connect(networkSession, &QNetworkSession::opened, this, &Server::sessionOpened);
		
		networkSession->open();
	}
	else {
		sessionOpened();
	}

	connect(tcpServer, &QTcpServer::newConnection, this, &Server::newConnection);
	connect(this, SIGNAL(requestReceived(QByteArray&)), votingCenter, SLOT(handleRequest(QByteArray&)));
}

void Server::sessionOpened()
{
	// Save the used configuration
	if (networkSession) {
		QNetworkConfiguration config = networkSession->configuration();
		QString id;
		if (config.type() == QNetworkConfiguration::UserChoice)
			id = networkSession->sessionProperty(QLatin1String("UserChoiceConfiguration")).toString();
		else
			id = config.identifier();

		QSettings settings(QSettings::UserScope, QLatin1String("QtProject"));
		settings.beginGroup(QLatin1String("QtNetwork"));
		settings.setValue(QLatin1String("DefaultNetworkConfiguration"), id);
		settings.endGroup();
	}

	tcpServer = new QTcpServer(this);
	if (!tcpServer->listen(HOST_ADDRESS, SERVER_PORT)) {
		qCritical() << tr("Unable to start the server: %1.").arg(tcpServer->errorString());
		return;
	}
	
	qDebug() << tr("The server is running on IP: %1, port: %2")
		.arg(HOST_ADDRESS.toString()).arg(tcpServer->serverPort());
}

Server::~Server()
{
	clientConnection->disconnectFromHost();
	tcpServer->close();
}

void Server::newConnection()
{
	clientConnection = tcpServer->nextPendingConnection();
	connect(clientConnection, &QIODevice::readyRead, this, &Server::read);

	connect(clientConnection, &QAbstractSocket::disconnected,
		clientConnection, &QObject::deleteLater);

	buffer.clear();
}

void Server::read()
{
	QByteArray incomingData = clientConnection->readAll();
	buffer.append(incomingData);

	while (buffer.length() >= HEADER_SIZE) 
	{
		HeaderSizeType blockSize;
		QDataStream stream(&buffer, QIODevice::ReadOnly);
		stream >> blockSize;
		
		if (blockSize + HEADER_SIZE > buffer.length())
		{
			return;
		}
		
		QByteArray block = buffer.mid(HEADER_SIZE, blockSize);
		emit requestReceived(block);
		buffer.remove(0, blockSize + HEADER_SIZE);
	}
}

void Server::answer(QByteArray &data)
{
	QByteArray block;
	QDataStream out(&block, QIODevice::WriteOnly);
	out << data;
	
	clientConnection->write(block);
}

void Server::answer(QString &data)
{
	QByteArray answerData;
	QDataStream answerStream(&answerData, QIODevice::WriteOnly);
	answerStream << data;

	answer(answerData);
}
