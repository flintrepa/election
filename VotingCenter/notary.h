﻿#pragma once

#include <rsa.h>
using CryptoPP::RSA;

class Notary
{
public:
	static Notary& getInstance()
	{
		static Notary instance;
		return instance;
	}

	RSA::PrivateKey getSK();
	RSA::PublicKey getPK();

private:
	Notary();
	Notary(Notary const&);// = delete;
	void operator=(Notary const&);// = delete;
	//~Notary();

	RSA::PrivateKey skN;
	RSA::PublicKey pkN;
};